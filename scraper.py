import feedparser, timeago, datetime 
from time import mktime
from flask import Flask

def scrape(source):
    try:
        d = feedparser.parse(source)
        feed = []
        for i in d.entries:
            x = d.entries.index(i)
            post = d.entries[x].title
            feed_title = d.feed.title
            date = datetime.datetime.fromtimestamp(mktime(d.entries[x].published_parsed))
            ago = timeago.format(date, datetime.datetime.now())
            url =  d.entries[x].link
            id_ = x
            feed.append(Item(post, feed_title, date, ago, url, id_))
        for y in feed:
            print(y)
    except Exception as e:
        print("Failed, see:")
        print(e)

class Item:
    def __init__(self, post, feed_title, date, ago, url, id_):
        self.post = post
        self.feed = feed_title
        self.ago = ago
        self.date = date
        self.url = url        
        self.id_ = id_ 
        self.isRead = False # Left here temporarily until I implement a proper isRead function
    def __str__(self):
        out = self.feed + "\n" + self.post + "\n" + self.ago + "\n" + self.url + "\n" + self.read + "\n ----------------------------"
        return out
    @property 
    def read(self):
        if self.isRead == False:
            return "Unread"
        elif self.isRead == True:
            return "Read"
            
print(scrape("https://news.ycombinator.com/rss"))        
